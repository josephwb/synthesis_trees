OPENTREE Synthesis Input
--------------
A repo storing source input for the OToL synthetic tree analyses.

##NOTE:
All previous files have been moved to the directory "OLD_FILES"; those files can be considered to do deprecated, although comparisons might be done with the past versions of the synthetic tree (in the directory "OLD_FILES/Synthetic_trees"). The directory "Source_info" contains information and source data for the current (and final!) set of synthesis analyses. 

##Downloading The Repository
From a Terminal, navigate to where you would like to put the repo. Now, type:

    git clone git@bitbucket.org:josephwb/synthesis_trees.git

To refresh the repo after an update, type:

    git pull


