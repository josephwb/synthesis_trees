OPENTREE Synthesis Input
--------------
A repo storing trees that are used as input for the OToL synthetic tree.

##NOTE:
Trees and taxonomy are currently in reciprocal flux, so expect changes. Trees are stored in directories labelled by which version of ott (OpenTree Taxonomy) they were processed with (e.g. trees-ott2.5draft1). The most recent synthesis sources trees are in the directory 'synth_source_trees-ott2.8draft5'. Synthetic and taxonomy trees are in the directory 'Synthetic_trees'.

##Downloading The Repository
From a Terminal, navigate to where you would like to put the repo. Now, type:

    git clone git@bitbucket.org:josephwb/synthesis_trees.git

To refresh the repo after an update, type:

    git pull

##File Info
Newick tree files are named as:

    prefix_studyid_treeid.tre

where _prefix_ designates where the study originated ('pg' = [phylografter](http://www.reelab.net/phylografter), 'ot' = the new [OpenTree curator app](http://tree.opentreeoflife.org/curator)), _studyid_ is the OpenTree study identifier, and _treeid_ is the OpenTree tree identifier. [Note: older trees do not have a prefix, as all originated from phylografter]. To see a particular study on the curator app, go to:

    http://tree.opentreeoflife.org/curator/study/view/prefix_studyid


